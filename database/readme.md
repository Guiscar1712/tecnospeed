## Before running the next steps, you need to have both PostgreSQL 10.13 or greater installed in your environment.
---
Install dependencies:
```bash
npm install
```
---

Create local database "xpro" with "localroot" user
```bash
psql postgres -U localhost -f ./setup.sql
```
or
```bash
psql -f ./setup.sql
```
*You can replace "postgres" with your local root user, if needed*

---

Run migrations in local environment
```bash
npx sequelize db:migrate
```

## The following steps are optional. Use with wisdom

Undo the last migration in local environment (one migration script at a time)
WARNING: There might be data loss
```bash
npx sequelize db:migrate:undo
```

Undo ALL migrations in local environment from the last until the specified one
WARNING: There might be data loss
```bash
npx sequelize db:migrate:undo:all --to XXXXXXXXXXXXXX-migration_name.js
```

Undo ALL migrations in local environment
WARNING: There might be data loss
```bash
npx sequelize db:migrate:undo:all
```

## Creating new migrations
```bash
npx sequelize migration:create --name=ACTION-TABLE
```
ACTION: create, alter, uk, constraint...
TABLE: Name of the table
