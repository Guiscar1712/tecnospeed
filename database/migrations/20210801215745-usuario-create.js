'use strict';

// @ts-ignore
var DataTypes = require('sequelize/lib/data-types');

module.exports = {
  // @ts-ignore
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('usuario', {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: DataTypes.INTEGER
        },
        id_carteira: {
          allowNull: false,
          type: DataTypes.INTEGER,
          references: {
            model: 'carteira',
            key: 'id'
          }
        },
        email: {
          allowNull: true,
          type: DataTypes.STRING(50)
        },
        ativo: {
          allowNull: false,
          defaultValue: true,
          type: DataTypes.BOOLEAN
        },
        nome: {
          allowNull: true,
          type: DataTypes.STRING(100)
        },
        data_criacao: {
          allowNull: false,
          type: DataTypes.DATE
        },
        data_alteracao: {
          allowNull: true,
          type: DataTypes.DATE
        }
      }, {
        freezeTableName: true,
        timestamps: true,
        createdAt: 'data_criacao',
        updatedAt: 'data_alteracao'
    })
  },

  // @ts-ignore
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('usuario');
  }
};
