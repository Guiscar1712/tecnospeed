'use strict';

// @ts-ignore
var DataTypes = require('sequelize/lib/data-types');

module.exports = {
  // @ts-ignore
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('usuario_login', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
      },
      login: {
        allowNull: false,
        type: DataTypes.STRING(255)
      },
      senha_hash: {
        allowNull: true,
        type: DataTypes.STRING(255)
      },
      token: {
        allowNull: true,
        type: DataTypes.STRING(255)
      },
      data_criacao: {
        allowNull: false,
        type: DataTypes.DATE
      },
      data_alteracao: {
        allowNull: true,
        type: DataTypes.DATE
      }
      }, {
        freezeTableName: true,
        timestamps: true,
        createdAt: 'data_criacao',
        updatedAt: 'data_alteracao'
    })
  },

  // @ts-ignore
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('usuario_login');
  }
};
