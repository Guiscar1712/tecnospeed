'use strict';

// @ts-ignore
var DataTypes = require('sequelize/lib/data-types');

module.exports = {
  // @ts-ignore
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('movimentacao', {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: DataTypes.INTEGER
        },
        nome: {
          allowNull: true,
          type: DataTypes.STRING(10)
        },
        id_carteira: {
          allowNull: false,
          type: DataTypes.INTEGER,
          references: {
            model: 'carteira',
            key: 'id'
          }
        },
        id_usuario: {
          allowNull: false,
          type: DataTypes.INTEGER,
          references: {
            model: 'usuario',
            key: 'id'
          }
        },
        valor: {
          allowNull: false,
          type: DataTypes.FLOAT,
        },
        data_criacao: {
          allowNull: false,
          type: DataTypes.DATE
        },
        data_alteracao: {
          allowNull: true,
          type: DataTypes.DATE
        }
      }, {
        freezeTableName: true,
        timestamps: true,
        createdAt: 'data_criacao',
        updatedAt: 'data_alteracao'
    })
  },

  // @ts-ignore
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('movimentacao');
  }
};
