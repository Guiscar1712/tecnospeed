'use strict';

// @ts-ignore
var DataTypes = require('sequelize/lib/data-types');

module.exports = {
  // @ts-ignore
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('carteira', {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: DataTypes.INTEGER
        },
        debito: {
          allowNull: true,
          type: DataTypes.FLOAT
        },
        credito: {
          allowNull: true,
          type: DataTypes.FLOAT
        },
        data_criacao: {
          allowNull: false,
          type: DataTypes.DATE
        },
        data_alteracao: {
          allowNull: true,
          type: DataTypes.DATE
        }
      }, {
        freezeTableName: true,
        timestamps: true,
        createdAt: 'data_criacao',
        updatedAt: 'data_alteracao'
    })
  },

  // @ts-ignore
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('usuario');
  }
};
