import { LoginController } from '@domains/auth/controller/loginController'
import { JwtMiddleware } from '@middle/jwt-middleware'
import 'dotenv/config'
import { IResultRequest } from "src/interfaces/result-request"
import { CatchControl } from 'src/utils/catch-control'

const express: any = require('express').Router()

express.post('/v1/login', async (req: any, res: any) => {

  try {
    const result: IResultRequest = await LoginController.login(req.body)
    res.status(result.status).json(result.content)
    console.log(result)
  } catch (e) {
    console.log(e)
    CatchControl.fromRoute(e, res)
  }

})

express.put('/v1/logout', JwtMiddleware.check, async (req: any, res: any) => {

  try {
    const result: IResultRequest = await LoginController.logout(req.colaborador_id)
    res.status(result.status).json(result.content)
  } catch (e) {
    CatchControl.fromRoute(e, res)
  }

})

module.exports = express;