import { UsuarioLoginController } from '@domains/usuario-login/controller/usuario-login-controller'
import { UsuarioController } from '@domains/usuario/controller/usuario-controller'
import 'dotenv/config'
import { UsuarioInterface } from 'src/interfaces/usuario-interface'

const express = require('express').Router()

express.get('/v1/login', async (req: any, res: any) => {
  try {
    const result: any = await UsuarioController.listarTodosOsUsuarios()
    return res.status(result.status).json(result.content)
  } catch (error) {
    return res.status(418).json(error)
  }
})

express.get('/v1/login/:id', async (req: any, res: any) => {
  try {
    const result: any = await UsuarioController.listarUsuarioPorId(req.params.id as number)
    return res.status(result.status).json(result.content)
  } catch (error) {
    return res.status(418).json(error)
  }
})

express.put('/v1/login', async (req: any, res: any) => {
  try {
    const result: any = await UsuarioController.AtualizarUsuario(req.body as UsuarioInterface)
    res.status(result.status).json(result.content)
  } catch (error) {
    return res.status(418).json(error)
  }
})

express.post('/v1/login', async (req: any, res: any) => {
  try {
    const result: any = await UsuarioController.CadastrarUsuario(req.body as UsuarioInterface)
    res.status(result.status).json(result.content)
  } catch (error) {
    return res.status(418).json(error)
  }
})

express.post('/v1/login-login', async (req: any, res: any) => {
  try {
    const result: any = await UsuarioLoginController.CadastrarUsuario(req.body)
    res.status(result.status).json(result.content)
  } catch (error) {
    return res.status(418).json(error)
  }
})

module.exports = express;