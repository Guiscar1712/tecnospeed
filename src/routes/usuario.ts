import 'dotenv/config'
import { UsuarioLoginController } from '@domains/usuario-login/controller/usuario-login-controller'
import { UsuarioController } from '@domains/usuario/controller/usuario-controller'
import { IResultRequest } from 'src/interfaces/result-request'
import { UsuarioInterface } from 'src/interfaces/usuario-interface'
import { CatchControl } from 'src/utils/catch-control'

const express = require('express').Router()

express.get('/v1/usuario', async (req: any, res: any) => {
  try {
    const result: IResultRequest = await UsuarioController.listarTodosOsUsuarios()
    return res.status(result.status).json(result.content)
  } catch (error) {
    return CatchControl.fromRoute(error, res)
  }
})

express.get('/v1/usuario/:id', async (req: any, res: any) => {
  try {
    const result: IResultRequest = await UsuarioController.listarUsuarioPorId(req.params.id as number)
    return res.status(result.status).json(result.content)
  } catch (error) {
    return CatchControl.fromRoute(error, res)
  }
})

express.put('/v1/usuario', async (req: any, res: any) => {
  try {
    const result: IResultRequest = await UsuarioController.AtualizarUsuario(req.body as UsuarioInterface)
    res.status(result.status).json(result.content)
  } catch (error) {
    return CatchControl.fromRoute(error, res)
  }
})

express.post('/v1/usuario', async (req: any, res: any) => {
  try {
    const result: IResultRequest = await UsuarioController.CadastrarUsuario(req.body as UsuarioInterface)
    res.status(result.status).json(result.content)
  } catch (error) {
    return CatchControl.fromRoute(error, res)
  }
})

express.post('/v1/usuario-login', async (req: any, res: any) => {
  try {
    console.log(req)
    const result: IResultRequest = await UsuarioLoginController.CadastrarUsuario(req.body)
    res.status(result.status).json(result.content)
  } catch (error) {
    CatchControl.fromRoute(error, res)
  }
})

module.exports = express;