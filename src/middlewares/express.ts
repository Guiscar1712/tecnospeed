import 'dotenv/config'
import express from 'express'
import cookieParser from 'cookie-parser'
import methodOverride from 'method-override'
import cors from 'cors'
import helmet from 'helmet'

var fileupload = require("express-fileupload");

const app = express()

module.exports = [
  express.urlencoded({ limit: '20MB', extended: true }),
  express.json({ limit: '20MB' }),
  express.raw({ limit: '20MB' }),
  express.text({ limit: '20MB' }),
  fileupload(),
  methodOverride('_method'),
  cookieParser(),
  cors(),
  helmet(),
]
