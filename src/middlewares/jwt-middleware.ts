import { JwtTokenDecoded } from '@domains/auth/services/jwt-token-decode';
import 'dotenv/config'
import JWT from 'jsonwebtoken'

export module JwtMiddleware {

  export function check(req: any, res: any, next: any){
    
    const token = (req.headers['authorization']) ? req.headers['authorization'].replace(/Bearer /g, ``) : null

    if (!token) {

      const auth: auth = { auth: false, message: 'No token provided.' }
      return res.status(401).send(auth);
    
    } else {
    
      JWT.verify(
        token,
        `${process.env.AUTH_SECRET}`,
        async (err: any, decoded: any) => {

          if (err) {
          
            const auth: auth = { auth: false, message: 'Failed to authenticate token.' }
            return res.status(500).send(auth);
          
          } else {
          
            const user: any = await JwtTokenDecoded(token)
            req.token = token
            req.colaborador_id = user.colaborador_id
            next()
          
          }
        }
      )
    }
  }
}

interface auth {
  auth: boolean,
  message: string
}