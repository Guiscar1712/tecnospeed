import 'dotenv/config'
import express from 'express'

const fs = require('fs');
const swaggerUi = require('swagger-ui-express')
const swaggerFile = require('../swagger_output.json')

const app = express().use(require('@middle/express'))

app.use('/doc', swaggerUi.serve, swaggerUi.setup(swaggerFile))

fs.readdirSync(require('path').join(__dirname, 'routes')).forEach((route: string) => {
  app.use('', require(`@routes/${route}`))
});

const server = app.listen(process.env.PORT, () => {
  console.log(`app is listening to port ${process.env.PORT}`)
})

server.timeout = 60000