import { DataTypes } from 'sequelize'
import { postgres } from '@services/postgres'

const tableName = 'tipo_movimentacao'

export const TipoMovimentacaoSchema = postgres.define(
  tableName,
  {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    nome: {
      allowNull: true,
      type: DataTypes.STRING(10)
    },
    data_criacao: {
      allowNull: false,
      type: DataTypes.DATE
    },
    data_alteracao: {
      allowNull: true,
      type: DataTypes.DATE
    }
  }, {
    freezeTableName: true,
    timestamps: true,
    createdAt: 'data_criacao',
    updatedAt: 'data_alteracao'
  }
)

export module TipoMovimentacaoQuery {

  export async function findAll() {
    return await TipoMovimentacaoSchema.findAll();
  }
}