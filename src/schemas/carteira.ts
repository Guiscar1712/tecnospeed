import { DataTypes } from 'sequelize'
import { postgres } from '@services/postgres'

const tableName = 'carteira'

export const CarteiraSchema = postgres.define(
  tableName, {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    debito: {
      allowNull: true,
      type: DataTypes.FLOAT
    },
    credito: {
      allowNull: true,
      type: DataTypes.FLOAT
    },
    data_criacao: {
      allowNull: false,
      type: DataTypes.DATE
    },
    data_alteracao: {
      allowNull: true,
      type: DataTypes.DATE
    }
  }, {
    freezeTableName: true,
    timestamps: true,
    createdAt: 'data_criacao',
    updatedAt: 'data_alteracao'
  }
)

export module CarteiraQuery {

  export async function findAll() {
    return await CarteiraSchema.findAll();
  }
}