import { DataTypes } from 'sequelize'
import { postgres } from '@services/postgres'

const tableName = 'movimentacao'

export const MovimentacaoSchema = postgres.define(
  tableName,
  {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    nome: {
      allowNull: true,
      type: DataTypes.STRING(50)
    },
    agendamento: {
      allowNull: true,
      type: DataTypes.DATE
    },
    repetir_pagamento: {
      allowNull: true,
      type: DataTypes.INTEGER
    },
    debito_automatico: {
      allowNull: true,
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    estorno: {
      allowNull: true,
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    id_carteira: {
      allowNull: false,
      type: DataTypes.INTEGER,
      references: {
        model: 'carteira',
        key: 'id'
      }
    },
    id_usuario: {
      allowNull: false,
      type: DataTypes.INTEGER,
      references: {
        model: 'usuario',
        key: 'id'
      }
    },
    valor: {
      allowNull: false,
      type: DataTypes.FLOAT,
    },
    observacao: {
      allowNull: false,
      type: DataTypes.STRING,
    },
    data_criacao: {
      allowNull: false,
      type: DataTypes.DATE
    },
    data_alteracao: {
      allowNull: true,
      type: DataTypes.DATE
    }
  }, {
    freezeTableName: true,
    timestamps: true,
    createdAt: 'data_criacao',
    updatedAt: 'data_alteracao'
  }
)

export module MovimentacaoQuery {

  export async function findAll() {
    return await MovimentacaoSchema.findAll();
  }
}