import { DataTypes } from 'sequelize'
import { postgres } from '@services/postgres'

const tableName = 'usuario_login'

export const UsuarioLoginSchema = postgres.define(
  tableName,
  {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    login: {
      allowNull: false,
      type: DataTypes.STRING(255)
    },
    senha_hash: {
      allowNull: true,
      type: DataTypes.STRING(255)
    },
    token: {
      allowNull: true,
      type: DataTypes.STRING(255)
    },
    data_criacao: {
      allowNull: false,
      type: DataTypes.DATE
    },
    data_alteracao: {
      allowNull: true,
      type: DataTypes.DATE
    }
  }, {
    freezeTableName: true,
    timestamps: true,
    createdAt: 'data_criacao',
    updatedAt: 'data_alteracao'
  }
)

export module UsuarioLoginQuery {

  export async function findAll() {
    return await UsuarioLoginSchema.findAll();
  }

  export async function listarPorLogin(login: string) :Promise<Array<any>> {
    return await UsuarioLoginSchema.findOne({ where: { login: login }, raw: true })
  }

  export async function salvarToken(token: string,  usuario_id: number) {
    await UsuarioLoginSchema.update( 
      { token: token, updated_at: postgres.fn('NOW') },
      { where: { id: usuario_id } 
    })
  }
}