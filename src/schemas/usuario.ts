import { DataTypes } from 'sequelize'
import { postgres } from '@services/postgres'

const tableName = 'usuario'

export const UsuarioSchema = postgres.define(
  tableName,
  {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    id_carteira: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    email: {
      allowNull: true,
      type: DataTypes.STRING(50)
    },
    ativo: {
      allowNull: false,
      defaultValue: true,
      type: DataTypes.BOOLEAN
    },
    nome: {
      allowNull: true,
      type: DataTypes.STRING(100)
    },
    data_criacao: {
      allowNull: false,
      type: DataTypes.DATE
    },
    data_alteracao: {
      allowNull: true,
      type: DataTypes.DATE
    }
  }, {
    freezeTableName: true,
    timestamps: true,
    createdAt: 'data_criacao',
    updatedAt: 'data_alteracao'
  }
)

export module UsuarioQuery {

  export async function findAll() {
    return await UsuarioSchema.findAll();
  }
}