import { DataTypes } from 'sequelize'
import { postgres } from '@services/postgres'

const tableName = 'historico'

export const HistoricoSchema = postgres.define(
  tableName,
  {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    id_carteira: {
      allowNull: false,
      type: DataTypes.INTEGER,
      references: {
        model: 'carteira',
        key: 'id'
      }
    },
    id_movimentacao: {
      allowNull: false,
      type: DataTypes.INTEGER,
      references: {
        model: 'movimentacao',
        key: 'id'
      }
    },
    id_usuario_pagante: {
      allowNull: false,
      type: DataTypes.INTEGER,
      references: {
        model: 'usuario',
        key: 'id'
      }
    },
    id_usuario_recebedor: {
      allowNull: false,
      type: DataTypes.INTEGER,
      references: {
        model: 'usuario',
        key: 'id'
      }
    },
    valor: {
      allowNull: false,
      type: DataTypes.FLOAT,
    },
    data_criacao: {
      allowNull: false,
      type: DataTypes.DATE
    },
    data_alteracao: {
      allowNull: true,
      type: DataTypes.DATE
    }
  }, {
    freezeTableName: true,
    timestamps: true,
    createdAt: 'data_criacao',
    updatedAt: 'data_alteracao'
  }
)

export module HistoricoQuery {

  export async function findAll() {
    return await HistoricoSchema.findAll();
  }
}