import { UsuarioSchema } from "src/schemas/usuario";
import { AbstractQuery } from "src/utils/abstract-query";

export async function UsuarioListarPorId(id: number, CAbstractQuery: any = AbstractQuery) {
  return await CAbstractQuery.listarPorId(id, UsuarioSchema);
}