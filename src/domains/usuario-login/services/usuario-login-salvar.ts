import { Op } from "sequelize";
import { UsuarioLoginInterface } from "src/interfaces/usuario-login-interface";
import { UsuarioLoginSchema } from "src/schemas/usuario-login";
import { AbstractQuery } from "src/utils/abstract-query";
import { Crypt } from "src/utils/crypt";

export async function UsuarioLoginSalvar (iUsuarioLogin: UsuarioLoginInterface, cAbstractQuery: any = AbstractQuery) {

  const usuarioJaExiste = await UsuarioLoginSchema.findAll({
      where: { id: { [Op.ne]: iUsuarioLogin.id }, login: iUsuarioLogin.login },
      raw: true
  })

  if (!iUsuarioLogin.login || iUsuarioLogin.login.length <= 0) {
    throw new Error('Preenha o campo E-mail corretamente');
  } else if (!iUsuarioLogin.senha || iUsuarioLogin.senha.length <= 0) {
    throw new Error('Preencha o campo senha corretamente');
  } else if (usuarioJaExiste && usuarioJaExiste.length > 0) {
    throw new Error('Usuário Já existe');
  }

  console.log(iUsuarioLogin, 'iUsuarioLogin')
  
  iUsuarioLogin.senha_hash = await Crypt.In(iUsuarioLogin.senha)

  iUsuarioLogin = await cAbstractQuery.salvar(iUsuarioLogin, UsuarioLoginSchema);
  
  return {status: 200, content: iUsuarioLogin }
}