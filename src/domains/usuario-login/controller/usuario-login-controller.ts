import { UsuarioLoginInterface } from "src/interfaces/usuario-login-interface";
import { UsuarioListarTodos } from "../services/usuario-login-listar";
import { UsuarioListarPorId } from "../services/usuario-login-listar-id";
import { UsuarioLoginSalvar } from "../services/usuario-login-salvar";

export module UsuarioLoginController {

  export async function listarTodosOsUsuarios() {
    return await UsuarioListarTodos()
  }

  export async function listarUsuarioPorId(id: number) {
    return await UsuarioListarPorId(id)
  }

  export async function CadastrarUsuario(usuario: UsuarioLoginInterface) {
    console.log(usuario)
    return await UsuarioLoginSalvar(usuario)
  }

  export async function AtualizarUsuario(usuario: UsuarioLoginInterface) {
    return await UsuarioLoginSalvar(usuario)
  }
}