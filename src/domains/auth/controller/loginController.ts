import 'dotenv/config'
import { Ilogin } from 'src/interfaces/login'
import { usuarioLogin } from '../services/login'
import { usuarioLoginValida } from '../services/login-validacao'
import { usuarioLogout } from '../services/logout'

export module LoginController {

  export async function login (login: Ilogin) {
    console.log(login)

    const authenticado = await usuarioLoginValida(login)

    return await usuarioLogin(authenticado)
  }

  export async function logout (req: any) {

    return usuarioLogout(req.usuario_id)
  }
}