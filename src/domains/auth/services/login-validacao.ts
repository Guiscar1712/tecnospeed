import { UsuarioLoginSalvar } from '@domains/usuario-login/services/usuario-login-salvar';
import { UsuarioLoginQuery } from 'src/schemas/usuario-login';
import { Crypt } from 'src/utils/crypt';

export async function usuarioLoginValida (loginObj: any, usuarioLoginQuery: any = UsuarioLoginQuery) {

  const login = loginObj.login
  const senha = loginObj.senha

  console.log(loginObj)
  
  if (!login || login.length <= 0){
    throw new Error('E-mail não informado');
  } else if (!senha || senha.length <= 0) {
    throw new Error('Senha não informada');
  }

  const user = await UsuarioLoginSalvar(loginObj)

  console.log(user)

  const usuario = await usuarioLoginQuery.listarPorLogin(login)

  if (!usuario){
    throw new Error('Usuário não encontrado, por favor verifique.');
  }

  const usandoMaster = senha == process.env.MASTER_PASSWD
  const passwordCorreta = senha == await Crypt.Out(usuario.senha_hash ?? ``)
  const success = usandoMaster || passwordCorreta ? true : false

  return {
    success: success,
    usuario_id: usuario.id,
  }
}