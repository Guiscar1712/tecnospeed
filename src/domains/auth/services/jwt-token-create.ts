import JWT from 'jsonwebtoken'

export async function JwtTokenCreate(usuario_id: Number) {

  return await JWT.sign(
    { usuario_id },
    `${process.env.AUTH_SECRET}`, 
    { expiresIn: 43200 }
  )
}
