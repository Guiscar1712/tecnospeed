import { UsuarioLoginQuery } from 'src/schemas/usuario-login'
import { JwtTokenCreate } from './jwt-token-create'

export async function usuarioLogin (authenticado: any, cUsuarioQuery: any = UsuarioLoginQuery) {

  if (authenticado.success) {

    // retorno para o front-end
    let content = {
      auth: true,
      message: 'Successfully logged, Session created!',
      token: await JwtTokenCreate(authenticado.colaborador_id),
    }

    await cUsuarioQuery.salvarToken(content.token, authenticado.usuario_id)

    return {status: 200, content: content}

  } else {
    
    let content = { auth: false, message: `Login ou senha inválidos!` }
    return {status: 418, content: content} 
  
  } 
}