import { UsuarioLoginQuery } from 'src/schemas/usuario-login'

export async function usuarioLogout (usuario_id: any, cUsuarioLoginQuery: any = UsuarioLoginQuery) {

  await cUsuarioLoginQuery.salvarToken(null, usuario_id)

  return {status: 200, content: { auth: false, token: null }} 
}