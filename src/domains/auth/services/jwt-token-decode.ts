import JWT from 'jsonwebtoken'

export async function JwtTokenDecoded(token: any) {

  return await JWT.verify(
    token,
    `${process.env.AUTH_SECRET}`,
    async (err: any, decoded: any) => {
      if (err) {
        return 'Token failed!'
      } else {
        return { colaborador_id: decoded.colaborador_id }
      }
    }
  )
}