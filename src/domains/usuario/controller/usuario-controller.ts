import { UsuarioInterface } from "src/interfaces/usuario-interface";
import { UsuarioListarTodos } from "../services/usuario-listar";
import { UsuarioListarPorId } from "../services/usuario-listar-id";
import { SalvarUsuario } from "../services/usuario-salvar";

export module UsuarioController {

  export async function listarTodosOsUsuarios() {
    return await UsuarioListarTodos()
  }

  export async function listarUsuarioPorId(id: number) {
    return await UsuarioListarPorId(id)
  }

  export async function CadastrarUsuario(usuario: UsuarioInterface) {
    return await SalvarUsuario(usuario)
  }

  export async function AtualizarUsuario(usuario: UsuarioInterface) {
    return await SalvarUsuario(usuario)
  }
}