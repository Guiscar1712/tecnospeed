import { UsuarioSchema } from "src/schemas/usuario";
import { AbstractQuery } from "src/utils/abstract-query";

export async function UsuarioListarTodos(CAbstractQuery: any = AbstractQuery) {
  return await CAbstractQuery.listarTodos(UsuarioSchema);
}