import { UsuarioInterface } from "src/interfaces/usuario-interface";
import { UsuarioSchema } from "src/schemas/usuario";
import { AbstractQuery } from "src/utils/abstract-query";



export async function SalvarUsuario(usuario: UsuarioInterface, CAbstractQuery: any = AbstractQuery) {

    if (!usuario.nome || usuario.nome.length <= 0) {
      throw new Error ('Preencha o campo "Nome" corretamente.')
    } else if (!usuario.email || usuario.email.length <= 0) {
      throw new Error ('Preencha o campo "E-mail" corretamente.')
    }

    return await CAbstractQuery.salvar(usuario, UsuarioSchema);
}