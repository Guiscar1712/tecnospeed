import { Op } from "sequelize";
import { AbsctractInterface } from "src/interfaces/abstract-interface";

export module AbstractQuery {

  export async function listarTodos(CQueryAbstract: any) {
    return await CQueryAbstract.findAll();
  }

  export async function listarPorId(id: number, CQueryAbstract: any) {
    return await CQueryAbstract.findOne({ where: { id: id }, raw: true });
  }

  export async function salvar(dadosAbstract: AbsctractInterface, CQueryAbstract: any) {
    if (!dadosAbstract.id || dadosAbstract.id <= 0) {
      dadosAbstract = await CQueryAbstract.create(dadosAbstract)
    } else {
      await CQueryAbstract.update(CQueryAbstract, { where: { id: dadosAbstract.id }})
    }

    return dadosAbstract
  }

  export async function listaDuplicadoPorNome (iDados: any, cGenericoSchema: any) :Promise<any> {
    return await cGenericoSchema.findAll(
      {
        where: { id: { [Op.ne]: [iDados.id] }, nome: iDados.nome },
        raw: true
      })
  }
}