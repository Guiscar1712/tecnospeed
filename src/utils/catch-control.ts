import { enumRequestResultStatus } from "src/enumerators/request-result-status"
import { IResultRequest } from "src/interfaces/result-request"

export module CatchControl {

  export async function fromRoute (exception: any, res: any) {

    if (exception.error) {
      
    }

    let message: string[] = (exception.message.split('|').length > 1) ? exception.message.split('|')[1] : exception.message
    const retorno: IResultRequest = {status: enumRequestResultStatus.error, content: message}
    res.status(418).json(retorno)
  }
}
