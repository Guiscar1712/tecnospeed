import CryptoJS from 'crypto-js'

export module Crypt {

  export async function In(data: any): Promise<string> {
    const key = `${process.env.AUTH_SECRET}`
    const encJson = CryptoJS.AES.encrypt( JSON.stringify(data), key).toString()
    return encJson
  }

  export function Out(data: any): JSON {
    const key = `${process.env.AUTH_SECRET}`
    const bytes = CryptoJS.AES.decrypt(data, key).toString(CryptoJS.enc.Utf8)
    return JSON.parse(bytes)
  }

}
