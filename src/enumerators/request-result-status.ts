export enum enumRequestResultStatus {
    success = 200,
    created = 201,
    forbidden = 403,
    error = 418,
}