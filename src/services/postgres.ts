import 'dotenv/config'
const { Sequelize } = require('sequelize')

const dialect = `${process.env.DB_PG_DIALECT}`
const user = `${process.env.DB_PG_USER}`
const host = `${process.env.DB_PG_HOST}`
const database = `${process.env.DB_PG_DATABASE}`
const password = `${process.env.DB_PG_PASSWD}`
const port = parseInt(`${process.env.DB_PG_PORT}`)
const schema = `${process.env.DB_PG_SCHEMA}`

export const postgres = new Sequelize(database, user, password, {
  dialect: dialect,
  host: host,
  port: port,
  logging: false,
  schema: schema,
  define: {
    "createdAt": "created_at",
    "updatedAt": "updated_at"
  }
})
