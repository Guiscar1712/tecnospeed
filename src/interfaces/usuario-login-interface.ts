import { AbsctractInterface } from "./abstract-interface";

export interface UsuarioLoginInterface extends AbsctractInterface {
    id: number
    login: string
    senha: string
    senha_hash: string
    token: string
    data_criacao: Date
    data_alteracao: Date
}