import { AbsctractInterface } from "./abstract-interface";

export interface UsuarioInterface extends AbsctractInterface {
    id_carteira: number
    email: string
    nome: string
    ativo: boolean
}