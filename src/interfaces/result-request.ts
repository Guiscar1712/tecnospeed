import { enumRequestResultStatus } from "src/enumerators/request-result-status";

export interface IResultRequest {
  status: enumRequestResultStatus,
  content: any
}