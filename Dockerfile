FROM node:14.15.3

WORKDIR /user/app
COPY package.json yarn.lock ./

RUN npm install

COPY . .

EXPOSE 3000
CMD ["npm", "start"]